64MON
======
64MON is used in the book "C64 Programmer's Reference Guide"

#### Download

Download the cartridge version from one of links shown below

[http://www.planetemu.net/rom/commodore-c64-applications-crt/64mon-v1-03-19xx](http://www.planetemu.net/rom/commodore-c64-applications-crt/64mon-v1-03-19xx)

[http://thegamearchives.net/?val=0_2_1_0_0_9_23743_0_0_0_0](http://thegamearchives.net/?val=0_2_1_0_0_9_23743_0_0_0_0)

#### Run

From Vice attach the cartridge image, then execute

	sys 32820
	
[Found here](https://groups.google.com/forum/#!topic/comp.sys.cbm/eOJD8cYWbvs)

To show the menu press the `F7` key

#### Convert `crt` to `prg` to `d64`

From Vice open Monitor (Machine -> Monitor) save to `prg` 

	S "/Users/dave/c64stuff/64mon.prg" 0 8000 9FFF

[Found here](http://www.lemon64.com/forum/viewtopic.php?t=15901&sid=d67b4d88e58c77dc5e2a859fff345e38)

Then create the `d64` using the tool presents on Vice (`vice/tools`)

	c1541 -format diskname,id d64 64mon.d64 -attach 64mon.d64 -write 64mon.prg 64mon

[Found here](http://codebase64.org/doku.php?id=base:tools_for_putting_files_into_a_.d64_image)

