test
	;lda #$09
	;jsr calc_pos
	;lda #>row_pos_hi
	;ldy #<row_pos_lo
	lda #$ff
	sta $fb
	lda #$04
	sta $fc
	ldy #$04
	lda #$80
	sta ($fb), y
	rts

; multiply A * 40
calc_pos
	tay
	sta mul_result_lo
	lda #$00
	sta mul_result_hi
	ldx #$05
	loop:
	asl mul_result_lo
	rol mul_result_hi
	clc
	dex
	bne loop

	tya
	sta mul_result_lo_sum
	lda #$00
	sta mul_result_hi_sum
	ldx #$03
	loop1:
	asl mul_result_lo_sum
	rol mul_result_hi_sum
	dex
	bne loop1

	lda #$00
	sta row_pos_lo
	lda #$04
	sta row_pos_hi

	lda mul_result_lo
	adc mul_result_lo_sum
	bcc no_c
	clc
	inc row_pos_hi
no_c	
	adc row_pos_lo
	sta row_pos_lo

	lda mul_result_hi
	adc mul_result_hi_sum
	adc row_pos_hi
	sta row_pos_hi
	rts

*=$c100

mul_result_lo .byte 0
mul_result_hi .byte 0
mul_result_lo_sum .byte 0
mul_result_hi_sum .byte 0

row_pos_lo .byte 0
row_pos_hi .byte 0


