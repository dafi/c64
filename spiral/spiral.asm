start
    jsr clear_screen
    jsr clear_env
    lda #$00
    sta $fb
    lda #$04
    sta $fc

    ldx #$00
    ldy #$00
main_loop
    lda min_x
    cmp max_x
    bne test_y
    jmp end_spiral
test_y
    lda min_y
    cmp max_y
    bne input
    jmp end_spiral

input
;    jsr GETIN
;    beq input
;    cmp #$20
;    bne check_right
;    jmp end_spiral
    lda #$60
    cmp $d012
    bne input

; move to right
check_right
    lda #MOVE_RIGHT
    cmp direction
    bne check_down
    lda curr_col
    cmp max_x
    bcs move_next_row
    jsr fill_pos
    inc curr_col
    jmp main_loop
move_next_row
    dec curr_col
    inc curr_row
    jsr inc_screen_row
    inc min_y
    lda #MOVE_DOWN
    sta direction
    jmp main_loop

check_down
    lda #MOVE_DOWN
    cmp direction
    bne check_left
    lda curr_row
    cmp max_y
    bcs move_next_row_2
    jsr fill_pos
    inc curr_row
    jsr inc_screen_row
    jmp main_loop
move_next_row_2
    dec curr_row
    dec curr_col
    jsr dec_screen_row
    dec max_x
    lda #MOVE_LEFT
    sta direction
    jmp main_loop

; move to left    
check_left
    nop
    nop
    lda #MOVE_LEFT
    cmp direction
    bne check_up
    lda curr_col
    cmp min_x
    bcs draw_pos
change_direction
    dec max_y
    inc curr_col
    dec curr_row
    jsr dec_screen_row
    lda #MOVE_UP
    sta direction
    jmp main_loop
draw_pos
    jsr fill_pos
    dec curr_col
    bmi change_direction
    jmp main_loop

; move to up
check_up
    lda #MOVE_UP
    cmp direction
    beq handle_move_up
    jmp main_loop
handle_move_up
    lda curr_row
    cmp min_y
    bcs draw_up
    inc curr_col
change_direction_1
    inc min_x
    inc curr_row
    jsr inc_screen_row
    lda #MOVE_RIGHT
    sta direction
    jmp main_loop
draw_up
    jsr fill_pos
    jsr dec_screen_row
    dec curr_row
    bmi change_direction_1
    jmp main_loop

end_spiral
    rts

fill_pos
    lda step
    cmp #$3a
    bne fill_pos_1
    lda #$30
    sta step
fill_pos_1    
    ldy curr_col
    sta ($fb), y
    inc step
    clc
    rts

clear_env
    lda #$00
    sta min_x
    sta min_y
    sta curr_col
    sta curr_row

    lda #MAX_COLS
    sta max_x
    lda #MAX_ROWS
    sta max_y

    lda #MOVE_RIGHT
    sta direction

    lda #$30
    sta step
    rts

GETIN = $FFE4

MAX_COLS = 33
MAX_ROWS = 21

;MAX_ROWS = 4
;MAX_COLS = 5

MOVE_LEFT = 6
MOVE_RIGHT = 7
MOVE_UP = 8
MOVE_DOWN = 9

min_x .byte 0
min_y .byte 0
max_x .byte MAX_COLS
max_y .byte MAX_ROWS
direction .byte MOVE_RIGHT
step .byte $30
curr_col .byte 0
curr_row .byte 0

