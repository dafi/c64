; Clear the screen
clear_screen
    ldx #$00
    lda #$20
_loop
    sta $0400,x
    sta $0500,x
    sta $0600,x
    sta $06e8,x
    inx
    bne _loop
    rts

; Increment by one a screen row (40 lines)
; The row address is stored on $FB-$FC
inc_screen_row
    clc
    lda $fb
    adc #$28
    sta $fb
    bcc _end
    inc $fc
_end
    rts

; Decrement by one a screen row (40 lines)
; The row address is stored on $FB-$FC
dec_screen_row
    ; before subtraction you need to set the carry
    ; https://www.dwheeler.com/6502/oneelkruns/asm1step.html
    ; Arithmetic Instructions
    sec
    lda $fb
    sbc #$28
    sta $fb
    bcs _end
    dec $fc
_end
    rts
